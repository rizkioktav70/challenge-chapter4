const express = require("express")
const path = require("path")
const app = express()
//routing
app.use(express.static('public'))

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, '/index.html'))
})

app.get('/cars', function (req, res) {
    res.sendFile(path.join(__dirname, '../public/cars.html'))
})

app.listen(3000, '0.0.0.0', () => {
    console.log("Server sudah berjalan, buka http://0.0.0.0:%d", 3000)
})