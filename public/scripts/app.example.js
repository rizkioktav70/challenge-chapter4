class App {
  constructor() {
    this.loadButton = document.getElementById("submit");
    this.carContainerElement = document.getElementById("cars-container");
  }

  async init() {
    await this.load();

  }

  run = () => {
    Car.list.forEach((car) => {
      const node = document.createElement("div");
      node.innerHTML = car.render();
      this.carContainerElement.appendChild(node);
    });
  };

  async load() {
    this.clear();
    const cars = await Binar.listCars();
    let inputTime = document.getElementById("inputTime").value;
    let inputDate = document.getElementById("inputDate").value;
    let inputDateTime = inputDate + "T" + inputTime + "Z";
    let inputCapacity = document.getElementById("passenger").value;
    let filteredCars = cars.filter((car) => {
      if (inputCapacity === "") {
        return car.available === true && Date.parse(car.availableAt) > Date.parse(inputDateTime);
      } else {
        return car.available === true && Date.parse(car.availableAt) > Date.parse(inputDateTime) && car.capacity >= inputCapacity;
      }
    });

    // alert apabila tombol cari di klik tanpa memasukan data waktu dan tanggal
    if (inputTime == "" || inputDate == "") {
      alert("Masukkan data pesanan terlebih dahulu");
      return false;
    }
    Car.init(filteredCars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
