/*
 * Contoh kode untuk membaca query parameter,
 * Siapa tau relevan! :)
 * */

const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());

// Coba olah data ini hehe :)
console.log(params);

/*
 * Contoh penggunaan DOM di dalam class
 * */
const app = new App();

const submit = document.getElementById('submit');
submit.addEventListener('click', () => {
    app.init().then(app.run);
})

function activatedbg() {
    document.getElementById("bgdark").style.width = "100%"
    document.getElementById("btn").style.opacity = "0";
    document.getElementById("form").style.opacity = "100%";
}

function nonactivatedbg() {
    document.getElementById("bgdark").style.width = "0"
    document.getElementById("btn").style.opacity = "100%";
    document.getElementById("form").style.opacity = "0";
}